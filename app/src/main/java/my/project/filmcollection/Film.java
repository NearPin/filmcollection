package my.project.filmcollection;

import java.util.List;

public class Film {
    public Boolean isFavorite = false;
    public String _id;
    public String Title;
    public String Year;
    public String Rated;
    public String Released;
    public String Runtime;
    public String Genre;
    public String Director;
    public String Writer;
    public String Actors;
    public String Plot;
    public String Language;
    public String Country;
    public String Awards;
    public String Poster;
    public List<RatingsClass> Ratings;
    public String Metascore;
    public String imbdRating;
    public String imbdVotes;
    public String imbdId;
    public String Type;
    public String DVD;
    public String BoxOffice;
    public String Production;
    public String Website;
    public String Response;
    public int Timing;
    public int getRuntime(String runtime){
        String[] values = runtime.split(" ");
        return Integer.parseInt(values[0]);
    }
}
class RatingsClass{
    public String Source;
    public String Value;
}
