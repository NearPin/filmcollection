package my.project.filmcollection;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;

public class FilmNetwork {
    public String createURL(String id){
        return "http://www.omdbapi.com/?i="+ id +"&apikey=b42a270e";
    }
    protected String getSJsonFromURL(URL url) throws IOException {

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
        String json = reader.readLine();
        return json;
    }
    protected Film getFilmFromJson(String json){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Film conceteFilm = gson.fromJson(json,Film.class);
        conceteFilm.Timing = new Random().nextInt(100);
        return conceteFilm;
    }
    public LinkedList<Film> Search(int runtime, LinkedList<Film> films){
        LinkedList<Film> list = new LinkedList<Film>();
        for (Film f:films){
            if (f.getRuntime(f.Runtime)>=runtime){
                list.add(f);
            }
        }
        Comparator<Film> comparator = new FilmTittleComparator().thenComparing(new FilmWriterComparator());
        Collections.sort(list,comparator);
        return list;
    }

    public void insertFilmIntoDatabase(Film film, Context context){
        SQLiteOpenHelper helper = new DatabaseHelper(context,DatabaseHelper.DB_NAME);
        SQLiteDatabase database = helper.getWritableDatabase();
        database = helper.getWritableDatabase();
        database.execSQL("INSERT INTO FILM_LIST (TITLE,YEAR,WRITER,ACTORS,RUNTIME,IS_FAVORITE) VALUES ('" + film.Title + "', '" + film.Year + "', '" + film.Writer + "', '" + film.Actors + "', '" + film.Runtime + "', '" + favoriteToString(film.isFavorite)+ "');");
        database.close();
    }
    public LinkedList<Film> getFavoriteFilmListFromDatabase(Context context){
        SQLiteOpenHelper helper = new DatabaseHelper(context,DatabaseHelper.DB_NAME);
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM FILM_LIST WHERE IS_FAVORITE = \"true\";",null);
        LinkedList<Film> list = getFilmListFromQuery(cursor);
        database.close();
        return list;
    }
    private LinkedList<Film> getFilmListFromQuery(Cursor cursor){
        LinkedList<Film> list = new LinkedList<Film>();
        if(cursor.moveToFirst()){
            while (cursor.moveToNext()){
                Film film = new Film();
                film._id = cursor.getString(0);
                film.Title = cursor.getString(1);
                film.Year = cursor.getString(2);
                film.Writer = cursor.getString(3);
                film.Actors = cursor.getString(4);
                film.Runtime = cursor.getString(5);
                String favoriteString  = cursor.getString(6);
                film.isFavorite = favoriteToBoolean(favoriteString);
                list.add(film);
            }
        }
        return list;
    }
    public LinkedList<Film> getFilmListFromDatabase(Context context){
        SQLiteOpenHelper helper = new DatabaseHelper(context,DatabaseHelper.DB_NAME);
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM FILM_LIST;",null);
        LinkedList<Film> list = getFilmListFromQuery(cursor);
        database.close();
        return list;

    }
    public void deleteFilmFromDatabase(Context context,int id){
        SQLiteOpenHelper helper = new DatabaseHelper(context,DatabaseHelper.DB_NAME);
        SQLiteDatabase database = helper.getWritableDatabase();
        database.execSQL("DELETE FROM FILM_LIST WHERE _id = " + id);
    }
    public boolean favoriteToBoolean(String str){
        if (str.equals("true"))
            return true;
        return false;
    }
    public String favoriteToString(boolean bool){
        if(bool)
            return "true";
        return "false";
    }
    public void setFavorite(Context context,int id,boolean bool){
        SQLiteOpenHelper helper = new DatabaseHelper(context,DatabaseHelper.DB_NAME);
        SQLiteDatabase database = helper.getWritableDatabase();
        database.execSQL("UPDATE FILM_LIST SET IS_FAVORITE = \"" + favoriteToString(bool) + "\" WHERE _id = " + id);
    }
}
