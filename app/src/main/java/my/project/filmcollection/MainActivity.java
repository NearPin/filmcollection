package my.project.filmcollection;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.LinkedList;

public class MainActivity extends Activity implements SeekBar.OnSeekBarChangeListener {
    private LinkedList<Film> films = new LinkedList<Film>();
    private FilmNetwork network = new FilmNetwork();
    private SeekBar seekBar;
    private boolean isFavorite = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SQLiteDatabase db = new DatabaseHelper(this,DatabaseHelper.DB_NAME).getWritableDatabase();
        db.close();
        //db.execSQL("DROP TABLE FILM_LIST");
        Intent parentIntent = getIntent();
        boolean isFirstStart = parentIntent.getBooleanExtra(ChoiceActivity.IS_FIRST_START,true);
        if(isFirstStart){
            Intent intent = new Intent(this,ChoiceActivity.class);
            startActivity(intent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        films = new LinkedList<Film>();

        seekBar = (SeekBar) findViewById(R.id.seekRuntime);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setProgress(1);
        showFilmList(network.Search(seekBar.getProgress(),films));

    }

    @Override
    protected void onStart() {
        super.onStart();
        films = network.getFilmListFromDatabase(getApplicationContext());
        showFilmList(network.Search(seekBar.getProgress(),films));
    }

    private void showFilmList(LinkedList<Film> filterList){
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);
        FilmAdapter adapter = new FilmAdapter(filterList);
        recyclerView.setAdapter(adapter);

    }

    class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder>{
        LinkedList<Film> list;
        FilmAdapter(LinkedList list){
            this.list = list;
        }
        @Override
        public FilmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
            TextView textView = (TextView) layoutView.findViewById(R.id.nameOfFilm);
            textView.setOnClickListener(new RecyclerOnClickListener(layoutView));
            ImageView imageView = (ImageView) layoutView.findViewById(R.id.isFavoriteMipMap);
            imageView.setOnClickListener(new isFavoriteOnClickListener(layoutView));
            return new ViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(FilmAdapter.ViewHolder holder, int position) {
            Film film = list.get(position);
            holder.textView.setText(film.Title);
            if(film.isFavorite){
                holder.imageView.setImageResource(R.mipmap.ic_is_favorite_true);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView textView;
            ImageView imageView;
            public ViewHolder(View itemView) {
                super(itemView);
                textView = (TextView) itemView.findViewById(R.id.nameOfFilm);
                imageView = (ImageView) itemView.findViewById(R.id.isFavoriteMipMap);
            }


        }
        class RecyclerOnClickListener implements View.OnClickListener{
            private View layoutView;
            public RecyclerOnClickListener(View view){
                layoutView = view;
            }

            @Override
            public void onClick(View view) {
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                int position = recyclerView.getChildLayoutPosition(layoutView);
                Film choisedFilm = list.get(position);
                Intent intent = new Intent(getApplicationContext(),FilmDetail.class);
                intent.putExtra(FilmDetail.TITTLE,choisedFilm.Title);
                intent.putExtra(FilmDetail.YEAR,choisedFilm.Year);
                intent.putExtra(FilmDetail.WRITER,choisedFilm.Writer);
                intent.putExtra(FilmDetail.ACTORS,choisedFilm.Actors);
                intent.putExtra(FilmDetail.TIMING,choisedFilm.Timing);
                intent.putExtra(FilmDetail.RUNTIME,choisedFilm.Runtime);
                intent.putExtra(FilmDetail.POSITION,position);
                intent.putExtra(FilmDetail.ID,choisedFilm._id);
                startActivityForResult(intent,1);
            }
        }
        class isFavoriteOnClickListener implements View.OnClickListener{
            private View layoutView;
            public isFavoriteOnClickListener(View view){
                layoutView = view;
            }

            @Override
            public void onClick(View v) {
                ImageView imageView = (ImageView) v;
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                int position = recyclerView.getChildLayoutPosition(layoutView);
                Film choisedFilm = list.get(position);
                if(choisedFilm.isFavorite==false){
                    choisedFilm.isFavorite = true;
                    imageView.setImageResource(R.mipmap.ic_is_favorite_true);
                    network.setFavorite(getApplicationContext(),Integer.parseInt(choisedFilm._id),true);
                } else{
                    choisedFilm.isFavorite = false;
                    imageView.setImageResource(R.mipmap.ic_is_favorite_false);
                    network.setFavorite(getApplicationContext(),Integer.parseInt(choisedFilm._id),false);
                }
            }
        }

    }

    public void favoriteOnClick(View view){
        isFavorite = !isFavorite;
        if(isFavorite){
            showFilmList(network.Search(seekBar.getProgress(),network.getFavoriteFilmListFromDatabase(getApplicationContext())));
        }else{
            showFilmList(network.Search(seekBar.getProgress(),films));
        }

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        TextView textView = (TextView) findViewById(R.id.time);
        textView.setText(seekBar.getProgress() + " min");
        if(isFavorite){
            showFilmList(network.Search(seekBar.getProgress(),network.getFavoriteFilmListFromDatabase(getApplicationContext())));
        }else{
            showFilmList(network.Search(seekBar.getProgress(),films));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data==null) {
            return;
        }
        int position = data.getIntExtra(FilmDetail.POSITION,0);
        int timing = data.getIntExtra(FilmDetail.TIMING,0);
        Film proxyFilm = films.get(position);
        proxyFilm.Timing = timing;
        films.set(position,proxyFilm);
        showFilmList(network.Search(seekBar.getProgress(),films));
    }
}