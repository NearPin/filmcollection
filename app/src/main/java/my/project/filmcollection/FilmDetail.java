package my.project.filmcollection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class FilmDetail extends Activity implements SeekBar.OnSeekBarChangeListener {

    public static final String TITTLE = "TITTLE";
    public static final String YEAR = "YEAR";
    public static final String WRITER = "WRITER";
    public static final String ACTORS = "ACTORS";
    public static final String TIMING = "TIMING";
    public static final String POSITION = "POSITION";
    public static final String RUNTIME = "RUNTIME";
    public static final String ID = "_id";
    private SeekBar seekBar;
    private int position;
    private int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);


        Intent intent = getIntent();
        position = intent.getIntExtra(POSITION,0);
        TextView textView = (TextView) findViewById(R.id.detailFilmInfo);
        textView.append("\n Name of Film : " + intent.getStringExtra(TITTLE) + "\n");
        textView.append("\n Year : " + intent.getStringExtra(YEAR) + "\n");
        textView.append("\n Writer : " + intent.getStringExtra(WRITER) + "\n");
        textView.append("\n Actors : " + intent.getStringExtra(ACTORS) + "\n");
        textView.append("\n Runtime :" + intent.getStringExtra(RUNTIME) + "\n");
        TextView textProgress = (TextView) findViewById(R.id.progressTiming);
        textProgress.setText(String.valueOf(intent.getIntExtra(TIMING,0)) + " min");

        id = Integer.parseInt(intent.getStringExtra(ID));

        seekBar = (SeekBar) findViewById(R.id.timingSeek);
        seekBar.setMax(new Film().getRuntime(intent.getStringExtra(RUNTIME)));
        seekBar.setProgress(intent.getIntExtra(TIMING,0));
        seekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        TextView textProgress = (TextView) findViewById(R.id.progressTiming);
        textProgress.setText(String.valueOf(seekBar.getProgress()) + " min");
        Intent intent = new Intent();
        intent.putExtra(TIMING,seekBar.getProgress());
        intent.putExtra(POSITION,position);
        setResult(RESULT_OK,intent);
    }
    public void deleteButtonOnClick(View view){
        new FilmNetwork().deleteFilmFromDatabase(getApplicationContext(),id);
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
