package my.project.filmcollection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "films";
    public DatabaseHelper(Context context,String DB_NAME) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT  EXISTS FILM_LIST( _id INTEGER PRIMARY KEY AUTOINCREMENT,TITLE TEXT,YEAR TEXT,WRITER TEXT,ACTORS TEXT,RUNTIME TEXT,IS_FAVORITE TEXT);");
        Film film = new Film();
        film.Title = "This film for currect work databse";
        film.Year = "123123";
        film.Writer = "some writer";
        film.Actors = "some acrots";
        film.Runtime = "22 min";
        film.isFavorite = true;
        db.execSQL("INSERT INTO FILM_LIST (TITLE,YEAR,WRITER,ACTORS,RUNTIME,IS_FAVORITE) VALUES ('" + film.Title + "', '" + film.Year + "', '" + film.Writer + "', '" + film.Actors + "', '" + film.Runtime + "', '" + new FilmNetwork().favoriteToString(film.isFavorite)+ "');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
