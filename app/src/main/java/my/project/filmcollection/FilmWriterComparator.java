package my.project.filmcollection;

import java.util.Comparator;

public class FilmWriterComparator implements Comparator<Film> {
    @Override
    public int compare(Film o1, Film o2) {
        return o1.Writer.compareTo(o2.Writer);
    }
}
