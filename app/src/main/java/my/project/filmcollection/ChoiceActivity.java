package my.project.filmcollection;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChoiceActivity extends AppCompatActivity {
    public static final String IS_FIRST_START = "IS_FIRST_START";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
    }
    public void findFilmChoiceOnClick(View view){
        Intent intent = new Intent(this,FindFilmActivity.class);
        startActivity(intent);
    }
    public void filmListChoiceOnClick(View view){
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(IS_FIRST_START,false);
        startActivity(intent);
    }
}
