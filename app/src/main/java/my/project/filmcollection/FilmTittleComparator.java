package my.project.filmcollection;

import java.util.Comparator;

public class FilmTittleComparator implements Comparator<Film> {
    @Override
    public int compare(Film o1, Film o2) {
        return o1.Title.compareTo(o2.Title);
    }
}
