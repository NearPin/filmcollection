package my.project.filmcollection;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FindFilmActivity extends Activity {

    private FilmNetwork network = new FilmNetwork();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_film);
    }
    public void editTextOnClick(View view){
        EditText text = (EditText) findViewById(R.id.editText);
        text.setText("");
    }

    public void sendButtonOnClick(View view){
        EditText editText = (EditText) findViewById(R.id.editText);
        try {
            new FilmQueryUrl().execute(new URL(network.createURL(editText.getText().toString())));

        }catch (MalformedURLException exception){exception.printStackTrace();}
    }

    private Film cocreteFilm;


    class FilmQueryUrl extends AsyncTask<URL,Void,Film> {
        @Override
        protected Film doInBackground (URL... urls){
            try {
                String json = network.getSJsonFromURL(urls[0]);
                cocreteFilm = network.getFilmFromJson(json);
            }catch (IOException e){
                new Toast(getApplicationContext()).makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
            return cocreteFilm;
        }

        @Override
        protected void onPostExecute(Film film) {
            super.onPostExecute(film);
            if(film.Title!=null){
               network.insertFilmIntoDatabase(film,getApplicationContext());
                new Toast(getApplicationContext()).makeText(getApplicationContext(),"Фильм добавлен",Toast.LENGTH_SHORT);
            }
        }
    }
}
